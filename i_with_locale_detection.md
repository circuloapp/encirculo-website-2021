---
permalink: i_with_locale_detection/
---

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', () => {
        const available_locales = {{ site.locales | jsonify }}
        let current_locale = navigator.languages.map(l => l.split('-')[0]).find(l => available_locales.includes(l))

        if (!current_locale) current_locale = 'en'

        window.location = `../${current_locale}/i/`
      })
    </script>
  </head>
  <body>
    <ul>
      <!-- Let visitors decide which locale they like to access -->
      {% for locale in site.locales %}
        <li><a href="../{{ locale }}/i/">{{ site.data[locale].locale_name }}</a></li>
      {% endfor %}
    </ul>
  </body>
</html>
