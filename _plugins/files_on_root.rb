# frozen_string_literal: true

# Moves the /i/ redirector and other files into the site's root.
Jekyll::Hooks.register :site, :post_write, priority: :low do |site|
  next unless site.config['default_locale'] == site.config['locale']

  site.config['files_on_root']&.each_pair do |origin, destination|
    dest = File.join(site.dest, '..', destination)

    FileUtils.rm_rf dest
    FileUtils.mv File.join(site.dest, origin), dest
  end
end
