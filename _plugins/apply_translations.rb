# frozen_string_literal: trueo

# A small plugin to set default values from Weblate.  Since
# jekyll-locales builds a site per locale, this will run once per
# locale.
Jekyll::Hooks.register :site, :post_read do |site|
  locales = site.config['locales']
  locale = site.config['locale']
  t = site.data[locale]

  unless t
    Jekyll.logger.warn "Missing _data/#{locale}.json file"
    next
  end

  site.config.tap do |c|
    c['title'] = t['website_name']
    c['tagline'] = t['website_headline_1']
    c['description'] = t['website_headline_2']
    c['dir'] = t['text_direction']
  end

  # Home page
  home = site.pages.find do |doc|
    doc.data['layout'] == 'home'
  end

  home&.data&.tap do |d|
    d['title'] = t['website_name']
    d['tagline'] = t['website_headline_1']
    d['description'] = t['website_headline_2']
  end

  # Community Resources page
  site.config['community_resources'] = community_resources = site.pages.find do |doc|
    doc.data['layout'] == 'community_resources'
  end

  community_resources&.data&.tap do |d|
    d['title'] = t['community_resources_headline']
    d['description'] = t['community_resources_content']
    d['permalink'] = t['community_resources_permalink']

    # Add translated permalink
    d['locales'] = locales.map do |l|
      {
        l => {
          'permalink' => site.data.dig(l, 'community_resources_permalink')
        }
      }
    end.inject(&:merge)
  end

  # Invitation page
  site.config['invitation'] = invitation = site.pages.find do |doc|
    doc.data['layout'] == 'invitation'
  end

  invitation&.data&.tap do |d|
    d['title'] = t['invitation_headline']
    d['description'] = t['invitation_content']
    d['permalink'] = t['invitation_permalink']

    # Add translated permalink
    d['locales'] = locales.map do |l|
      {
        l => {
          'permalink' => site.data.dig(l, 'invitation_permalink')
        }
      }
    end.inject(&:merge)
  end
end
